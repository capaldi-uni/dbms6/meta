-- ����� ������� � ������������� ����������� � �� � ������ ������
SELECT
	*
FROM
	users
JOIN users_to_achievements ON
	users.id = user_id
JOIN achievements ON
	achievements.id = achievement_id
WHERE
	achievements.id = 1
	AND users.last_ip NOT IN (
	SELECT
		ip
	FROM
		blacklist);
	

-- ������ ���������� ������������ � ��� ������
SELECT
	users.id,
	users.nickname,
	privileges.name,
	feedback.text
FROM
	users
JOIN PRIVILEGES ON
	privilege = privileges.id
LEFT JOIN feedback ON
	users.id = user_id
WHERE
	users.id = 7;


-- ������ �� ������ �� ������ ������������� ������ � ������� ��� � ���������
SELECT
	users.id,
	users.nickname,
	users.level,
	feedback.text
FROM
	users
JOIN feedback ON
	users.id = user_id
WHERE
	"level" >= 40
	AND users.last_ip NOT IN (
	SELECT
		ip
	FROM
		blacklist);


-- ������� ���������� ��� �������
SELECT
	COUNT(game_id),
	users.id,
	users.nickname
FROM
	users
JOIN games ON
	users.id = user_id
GROUP BY
	users.id
ORDER BY
	COUNT(game_id) DESC;


-- ��������� ���� �� ������������ � ����� ����� ��� � �������
SELECT
	EXISTS(
	SELECT
		id
	FROM
		users
	WHERE
		nickname = 'Vel00');
	

-- ������������ � ����� ������� �������
SELECT
	"level",
	nickname
FROM
	users
ORDER BY
	"level" DESC
LIMIT 1;


-- ������� ����������, ������� ���������� �� �������� ������������
SELECT
	"name"
FROM
	"privileges"
WHERE
	id > 1;


-- ������� ��� ������ � ip � ��������� ������� �� ������� ��� ���� ������
SELECT
	nickname,
	reason
FROM
	blacklist
JOIN users ON
	users.last_ip = blacklist.ip;
