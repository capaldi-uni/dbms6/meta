
-- ������� �������� �������� � ������ ������ ������-�� ��������(����� �������� ������� � ������������ ������)
 SELECT
	ship.name,
	sum(value) AS total_value
FROM
	ship
JOIN ship_to_deck ON
	ship.id = ship_id
JOIN deck ON
	deck_id = deck.id
JOIN armor ON
	armor_id = armor.id
GROUP BY
	ship.name
HAVING
	sum(value) > 200
ORDER BY 
	total_value DESC;


-- �������� ������� � �������, ������� �������� ���������� ��
 SELECT
	DISTINCT ship.name,
FROM
	ship
JOIN ship_to_deck ON
	ship.id = ship_id
JOIN deck ON
	deck_id = deck.id
WHERE
	default_life = 490;


-- ����� ������� � ������� ���� ������� �������� �����
 SELECT
	DISTINCT ship.name
FROM
	ship
JOIN ship_to_deck ON
	ship.id = ship_id
JOIN deck ON
	deck_id = deck.id
JOIN ammunition ON
	ammunition_id = ammunition.id
WHERE
	type_id IN (0, 2);


-- ������� ������� �� �������� �������
 SELECT
	ammunition.id,
	damage,
	aoe,
	ammunition_count,
	ammunition_type.name
FROM
	ship
JOIN ship_to_deck ON
	ship.id = ship_id
JOIN deck ON
	deck_id = deck.id
JOIN ammunition ON
	ammunition_id = ammunition.id
JOIN ammunition_type ON
	type_id = ammunition_type.id
WHERE
	ship.name = 'Vampire';


-- ����� ��� ������� ��������� �����
 SELECT
	DISTINCT ship.name
FROM
	ship
JOIN ship_to_deck ON
	ship.id = ship_id
JOIN deck ON
	deck_id = deck.id
JOIN color ON
	color.id = color_id
WHERE
	color.name = 'black';


-- ������� ������� � ��������� ����������� ��������� �� ���
 SELECT
	ship.name,
	SUM(ammunition_count)
FROM
	ship
JOIN ship_to_deck ON
	ship.id = ship_id
JOIN deck ON
	deck_id = deck.id
GROUP BY
	ship.name;


-- ������� ��� ������� ��������� ���������
 SELECT
	ship.name
FROM
	ship
JOIN environment_to_ship ON
	ship.id = ship_id
JOIN environment ON
	environment_id = environment.id
WHERE
	environment.name = 'Caribbean';


-- ������� ��� ��������� ��������� ��������� �������
 SELECT
	environment.name
FROM
	ship
JOIN environment_to_ship ON
	ship.id = ship_id
JOIN environment ON
	environment_id = environment.id
WHERE
	ship.name = 'Titanic';
