CREATE TABLE [environment] (
	id integer IDENTITY(0, 1) NOT NULL,
	name VARCHAR(255) NOT NULL,
	whater_color_id integer NOT NULL,
	earth_color_id integer NOT NULL,
	field_size integer NOT NULL,
  CONSTRAINT [PK_ENVIRONMENT] PRIMARY KEY CLUSTERED
  (
  [id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [color] (
	id integer IDENTITY(0, 1) NOT NULL,
	name VARCHAR(255) NOT NULL,
	R integer NOT NULL,
	G integer NOT NULL,
	B integer NOT NULL,
	A integer NOT NULL,
  CONSTRAINT [PK_COLOR] PRIMARY KEY CLUSTERED
  (
  [id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [ship] (
	id integer IDENTITY(0, 1) NOT NULL,
	name VARCHAR(255) NOT NULL,
  CONSTRAINT [PK_SHIP] PRIMARY KEY CLUSTERED
  (
  [id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [ammunition_type] (
	id integer IDENTITY(0, 1) NOT NULL,
	name VARCHAR(255) NOT NULL,
  CONSTRAINT [PK_AMMUNITION] PRIMARY KEY CLUSTERED
  (
  [id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [ammunition] (
	id integer IDENTITY(0, 1) NOT NULL,
	damage integer NOT NULL,
	aoe integer NOT NULL,
	type_id integer NOT NULL,
  CONSTRAINT [PK_AMMUNITION] PRIMARY KEY CLUSTERED
  (
  [id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [deck] (
	id integer IDENTITY(0, 1) NOT NULL,
	name VARCHAR(255) NOT NULL,
	crew_count integer NOT NULL,
	default_life integer NOT NULL,
	ammunition_count integer NOT NULL,
	color_id integer NOT NULL,
	armor_id integer NOT NULL,
	ammunition_id integer NOT NULL,
  CONSTRAINT [PK_DECK] PRIMARY KEY CLUSTERED
  (
  [id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [ship_to_deck] (
	id integer IDENTITY(0, 1) NOT NULL,
	ship_id integer NOT NULL,
	deck_id integer NOT NULL,
  CONSTRAINT [PK_SHIP_TO_DECK] PRIMARY KEY CLUSTERED
  (
  [id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [armor_type] (
	id integer IDENTITY(0, 1) NOT NULL,
	name VARCHAR(255) NOT NULL,
  CONSTRAINT [PK_ARMOR] PRIMARY KEY CLUSTERED
  (
  [id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [armor] (
	id integer IDENTITY(0, 1) NOT NULL,
	value integer NOT NULL,
	type_id integer NOT NULL,
  CONSTRAINT [PK_ARMOR] PRIMARY KEY CLUSTERED
  (
  [id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [environment_to_ship] (
	environment_id integer NOT NULL,
	ship_id integer NOT NULL,
  CONSTRAINT [PK_ENVIRONMENT_TO_SHIP] PRIMARY KEY CLUSTERED
  (
  [environment_id], [ship_id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
ALTER TABLE [environment] WITH CHECK ADD CONSTRAINT [environment_fk0] FOREIGN KEY ([whater_color_id]) REFERENCES [color]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [environment] CHECK CONSTRAINT [environment_fk0]
GO
ALTER TABLE [environment] WITH CHECK ADD CONSTRAINT [environment_fk1] FOREIGN KEY ([earth_color_id]) REFERENCES [color]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [environment] CHECK CONSTRAINT [environment_fk1]
GO




ALTER TABLE [deck] WITH CHECK ADD CONSTRAINT [deck_fk0] FOREIGN KEY ([color_id]) REFERENCES [color]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [deck] CHECK CONSTRAINT [deck_fk0]
GO
ALTER TABLE [deck] WITH CHECK ADD CONSTRAINT [deck_fk1] FOREIGN KEY ([armor_id]) REFERENCES [armor]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [deck] CHECK CONSTRAINT [deck_fk1]
GO
ALTER TABLE [deck] WITH CHECK ADD CONSTRAINT [deck_fk2] FOREIGN KEY ([ammunition_id]) REFERENCES [ammunition]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [deck] CHECK CONSTRAINT [deck_fk2]
GO

ALTER TABLE [ship_to_deck] WITH CHECK ADD CONSTRAINT [ship_to_deck_fk0] FOREIGN KEY ([ship_id]) REFERENCES [ship]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [ship_to_deck] CHECK CONSTRAINT [ship_to_deck_fk0]
GO
ALTER TABLE [ship_to_deck] WITH CHECK ADD CONSTRAINT [ship_to_deck_fk1] FOREIGN KEY ([deck_id]) REFERENCES [deck]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [ship_to_deck] CHECK CONSTRAINT [ship_to_deck_fk1]
GO


ALTER TABLE [environment_to_ship] WITH CHECK ADD CONSTRAINT [environment_to_ship_fk0] FOREIGN KEY ([environment_id]) REFERENCES [environment]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [environment_to_ship] CHECK CONSTRAINT [environment_to_ship_fk0]
GO
ALTER TABLE [environment_to_ship] WITH CHECK ADD CONSTRAINT [environment_to_ship_fk1] FOREIGN KEY ([ship_id]) REFERENCES [ship]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [environment_to_ship] CHECK CONSTRAINT [environment_to_ship_fk1]
GO


ALTER TABLE [ammunition] WITH CHECK ADD CONSTRAINT [ammunition_fk0] FOREIGN KEY ([type_id]) REFERENCES [ammunition_type]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [ammunition] CHECK CONSTRAINT [ammunition_fk0]
GO

ALTER TABLE [armor] WITH CHECK ADD CONSTRAINT [armor_fk0] FOREIGN KEY ([type_id]) REFERENCES [armor_type]([id])
ON UPDATE NO ACTION
GO
ALTER TABLE [armor] CHECK CONSTRAINT [armor_fk0]
GO