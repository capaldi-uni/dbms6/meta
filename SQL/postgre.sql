CREATE TABLE IF NOT EXISTS "privileges" (
    "id" serial NOT NULL,
    "name" varchar(32) NOT NULL,

    CONSTRAINT "privileges_pk"
        PRIMARY KEY ("id")
);


CREATE TABLE IF NOT EXISTS "users" (
    "id" serial NOT NULL,
    "nickname" varchar(32) NOT NULL UNIQUE,
    "pass_hash" varchar(32) NOT NULL,
    "last_ip" varchar(16) NOT NULL,
    "privilege" integer NOT NULL,
    "experience" integer NOT NULL,
    "level" integer NOT NULL,

    CONSTRAINT "users_pk"
        PRIMARY KEY ("id"),

    CONSTRAINT "users_fk0"
        FOREIGN KEY ("privilege")
        REFERENCES "privileges"("id")
);


CREATE TABLE IF NOT EXISTS "games" (
    "user_id" integer NOT NULL,
    "game_id" integer NOT NULL,

    CONSTRAINT "games_pk"
        PRIMARY KEY ("user_id","game_id"),

    CONSTRAINT "games_fk0"
        FOREIGN KEY ("user_id")
        REFERENCES "users"("id")
);


CREATE TABLE IF NOT EXISTS "achievements" (
    "id" serial NOT NULL,
    "name" varchar(255) NOT NULL,
    "icon" varchar(255) NOT NULL,
    "asset_id" varchar(255) NOT NULL,

    CONSTRAINT "achievements_pk"
        PRIMARY KEY ("id")
);


CREATE TABLE IF NOT EXISTS "users_to_achievements" (
    "user_id" integer NOT NULL,
    "achievement_id" integer NOT NULL,
    "acquirement_date" TIMESTAMP NOT NULL,

    CONSTRAINT "users_to_achievements_pk"
        PRIMARY KEY ("user_id","achievement_id"),

    CONSTRAINT "users_to_achievements_user"
        FOREIGN KEY ("user_id")
        REFERENCES "users"("id"),

    CONSTRAINT "users_to_achievements_achievement"
        FOREIGN KEY ("achievement_id")
        REFERENCES "achievements"("id")
);


CREATE TABLE IF NOT EXISTS "feedback" (
    "id" serial NOT NULL,
    "user_id" integer NOT NULL,
    "text" varchar(255) NOT NULL,

    CONSTRAINT "feedback_pk"
        PRIMARY KEY ("id"),

    CONSTRAINT "feedback_user"
        FOREIGN KEY ("user_id")
        REFERENCES "users"("id")
);


CREATE TABLE IF NOT EXISTS "blacklist" (
    "id" serial NOT NULL,
    "ip" varchar(16) NOT NULL,
    "reason" varchar(255) NOT NULL,
    "issuer" integer NOT NULL,

    CONSTRAINT "blacklist_pk"
        PRIMARY KEY ("id"),

    CONSTRAINT "blacklist_issuer"
        FOREIGN KEY ("issuer")
        REFERENCES "users"("id")
);
